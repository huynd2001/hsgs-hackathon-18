import React from "react";
import BigBoard from "./lib/Plumber.js";

import "./index.less";

class BigBoardBuddy extends React.Component 
{	
	render() 
	{
		let err = this.props.error
      ? this.props.error.message
      : "Giải câu đố thôi nào";
    if (this.props.isEnding !== null) {
      err = "Chúc mừng bạn đã thắng!!!"
    }
		let board = [];
		let N = this.props.state.PlayBoard.length;
		let M = this.props.state.PlayBoard[0].length;
		for(let i = 0; i < N ; i++)
		{
			for(let j = 0; j < M ; j++)
			{
				// Alright, finally
				const ButtImage0 = require('./button' + this.props.state.CurrMod + '/right/' + this.props.state.PlayBoard[i][j] + '.png');
				const ButtImage1 = require('./button' + this.props.state.CurrMod + '/wrong/' + this.props.state.PlayBoard[i][j] + '.png');
				// This will return the exact files I need
				if(this.props.state.BoolBoard[i][j] == 0)
				board.push(
							<button className = "button" 
							style = {
								{
									backgroundImage : `url(${ButtImage0})`,
								}
							}
							onClick={() => this.props.move({x : i, y : j})}>
							</button>
					);
				else
				board.push(
							<button className = "button" 
							style = {
								{
									backgroundImage : `url(${ButtImage1})`,
								}
							}
							onClick={() => this.props.move({x : i, y : j})}>
							</button>
					);
			}
			board.push(
			<br></br>
			);
		}
		const Butt = [];
		// Will have the button in it if the game has ended
		if(this.props.isEnding == "won")
		{
			Butt.push(
			<button className = "newbutt" onClick={() => this.props.NewGame()}>Làm ván mới?</button>
			);
		}
		return (
		<div class = "s43">
		
		{board}
		<button className = "changebutt" onClick={() => this.props.changeMod()}>
		Thử đường ống khác
		</button>
		<br></br>
		<button className = "solbutt" onClick={() => this.props.ShowSol()}>
		Gợi ý
		</button>
		<br></br>
		{Butt}
		<pre>{JSON.stringify(err)}</pre>
		</div>
		);
	}
}

export default BigBoardBuddy;